﻿using System;
using Pong.GameCore;
using UnityEngine;

public class Player : MonoBehaviour {
    public int Score { get; private set; }
    [NonSerialized]
    public PlayerSide playerSide;
    SpriteRenderer spriteRenderer;
    string inputString;

    float maxYPos;
    float minYPos;

    protected virtual void Start() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        inputString = string.Format("Vertical{0}", playerSide);
        var scale = Vector3.Scale(spriteRenderer.sprite.bounds.extents, transform.lossyScale);
        SetMinMaxYPosition(scale);
        SetSide(scale);
        Game.OnGameStart += HandleGameStart;
        Game.OnEndGame += HandleGameEnd;
    }

    protected virtual void OnDestroy() {
        Game.OnGameStart -= HandleGameStart;
        Game.OnEndGame -= HandleGameEnd;
    }

    void HandleGameStart() {
        Score = 0;
    }

    void HandleGameEnd() {
        transform.position = new Vector3(transform.position.x, 0);
    }

    public void IncrementScore() {
        Score++;
    }

    void SetMinMaxYPosition(Vector3 scale) {
        maxYPos = Game.CameraBounds.max.y - scale.y;
        minYPos = Game.CameraBounds.min.y + scale.y;
    }

    void SetSide(Vector3 spriteScale) {
        float x;
        if (playerSide == PlayerSide.Left)
            x = Game.CameraBounds.min.x + spriteScale.x;
        else
            x = Game.CameraBounds.max.x - spriteScale.x;
        transform.position = new Vector2(x, 0);
    }

    protected virtual float GetMove() {
        return Input.GetAxis(inputString);
    }

    void Update() {
        var move = GetMove();
        if(Game.isPlaying)
            transform.position = CalculatePosition(move);
    }

    Vector2 CalculatePosition(float direction) {
        var trans = direction * Game.Settings.playersMoveSpeed * Time.deltaTime;
        var newYPos = transform.position.y + trans;
        var clampPos = Mathf.Clamp(newYPos, minYPos, maxYPos);
        return new Vector2(transform.position.x, clampPos);
    }
}