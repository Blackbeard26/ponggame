﻿using System;
using Pong.GameCore;
using UnityEngine;

public class Hybrid : Player {
    [SerializeField]
    Transform ball;
    bool stopMove = true;
    bool ballBringCloser;
    float lastMove;
    Vector2 lastBallPosition;

    protected override void Start() {
        base.Start();
        Game.OnRoundStart += HandleStartRound;
        Game.OnEndRound += HandleEndRound;
    }

    protected override void OnDestroy() {
        base.OnDestroy();
        Game.OnRoundStart -= HandleStartRound;
        Game.OnEndRound -= HandleEndRound;
    }

    void FixedUpdate() {
        ballBringCloser = BallBringCloser();
    }

    protected override float GetMove() {
        if (Game.gameMode == GameMode.Singleplayer)
            return stopMove ? 0 : GetAIMove();
        else
            return base.GetMove();
    }

    float GetAIMove() {
        float move = 0f;
        if (ballBringCloser) {
            var newDir = ball.position.y > transform.position.y ? 1 : -1;
            var newMove = Mathf.Lerp(lastMove, newDir, Time.deltaTime * Game.Settings.aiLerpSpeed);
            lastMove = newMove;
            move = newMove;
        }
        return move;
    }

    bool BallBringCloser() {
        var result = Math.Abs(transform.position.x - ball.position.x) < Math.Abs(transform.position.x - lastBallPosition.x);
        lastBallPosition = ball.position;
        return result;
    }

    void HandleStartRound() {
        stopMove = false;
    }

    void HandleEndRound(Player player) {
        stopMove = true;
    }
}