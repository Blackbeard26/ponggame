﻿using UnityEngine;

namespace Pong.GameCore {
    [CreateAssetMenu(fileName = "GameSettings", menuName = "Pong/Game Settings", order = 1)]
    public class GameSettings : ScriptableObject {
        [Header("Game")]
        public float borderWidth;
        public float nextRoundDelay;
        public float positionPreviewTime;
        public int pointsToWin;
        [Header("Ball")]
        public float startBallMoveSpeed;
        public float maxBallMoveSpeed;
        public float ballAcceleration;
        public float changeBallDirForce;
        [Header("Player")]
        public float playersMoveSpeed;
        public float aiLerpSpeed;
    }
}