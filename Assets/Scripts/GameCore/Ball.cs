﻿using System.Collections;
using UnityEngine;

namespace Pong.GameCore {
    public class Ball : MonoBehaviour {
        [SerializeField]
        AudioClip hit;
        [SerializeField]
        AudioClip lose;
        [SerializeField]
        ParticleSystem particle;
        [SerializeField]
        TrailRenderer trail;
        Rigidbody2D rigid2D;
        SpriteRenderer spriteRenderer;
        CircleCollider2D circleCollider;
        AudioSource audioSource;
        float moveSpeed;
        public Vector2 v;

        void Start() {
            rigid2D = GetComponent<Rigidbody2D>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            circleCollider = GetComponent<CircleCollider2D>();
            audioSource = GetComponent<AudioSource>();
            Game.OnEndRound += HandleEndRound;
            Game.OnEndGame += HandleEndGame;
            trail.sortingLayerName = "Background";
            ActioveBall(false);
        }

        void OnDestroy() {
            Game.OnEndGame -= HandleEndGame;
            Game.OnEndRound -= HandleEndRound;
        }

        void FixedUpdate() {
            if (rigid2D.velocity.magnitude < Game.Settings.maxBallMoveSpeed) {
                moveSpeed += Time.fixedDeltaTime * Game.Settings.ballAcceleration;
                rigid2D.velocity = rigid2D.velocity.normalized * moveSpeed;
            }
        }

        void OnCollisionEnter2D(Collision2D collision) {
            if (collision.collider.tag == "Player") 
                ChangeDirectionAccordingToHitPoint(collision);
            if(collision.collider.tag != "ScoreBorder")
                audioSource.PlayOneShot(hit);
        }

        void ChangeDirectionAccordingToHitPoint(Collision2D collision) {
            var playerTransform = collision.collider.transform;
            var targetDir = transform.position - playerTransform.position;
            var multipler = AngleDir(playerTransform.right, targetDir) > 0 ? -1 : 1;
            var angle = multipler * Vector2.Angle(playerTransform.right, targetDir);
            rigid2D.velocity = Quaternion.AngleAxis(angle * Game.Settings.changeBallDirForce, Vector3.forward) * rigid2D.velocity;
        }

        float AngleDir(Vector2 A, Vector2 B) {
            return -A.x * B.y + A.y * B.x;
        }

        void HandleEndRound(Player player) {
            rigid2D.velocity = Vector2.zero;
            ShowDestroyEffect();
        }

        void HandleEndGame() {
            rigid2D.velocity = Vector2.zero;
            ActioveBall(false);
        }

        void ShowDestroyEffect() {
            ActioveBall(false);
            particle.Play();
            audioSource.PlayOneShot(lose);
        }

        private void ActioveBall(bool active) {
            spriteRenderer.enabled = active;
            circleCollider.enabled = active;
        }

        public void PrepareNewRound() {
            StartCoroutine(StartNewRound());
        }

        IEnumerator StartNewRound() {
            yield return new WaitForSeconds(Game.Settings.nextRoundDelay);
            if (Game.isPlaying) {
                transform.position = GetRandomStartPosition();
                ActioveBall(true);
                yield return new WaitForSeconds(Game.Settings.positionPreviewTime);
                if (Game.OnRoundStart != null)
                    Game.OnRoundStart();
                rigid2D.velocity = GetStartDirection();
            }
        }

        Vector2 GetStartDirection() {
            moveSpeed = Game.Settings.startBallMoveSpeed;
            return new Vector2(Random.Range(-1f, 1f) > 0 ? 1: -1, Random.Range(-1f, 1f)) * moveSpeed;
        }

        Vector2 GetRandomStartPosition() {
            var spriteSpize = Vector2.Scale(transform.lossyScale, spriteRenderer.sprite.bounds.size);
            var range = Game.CameraBounds.extents.y - spriteSpize.y;
            return new Vector2(0, Random.Range(-range, range));
        }
    }
}