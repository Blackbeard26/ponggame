﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pong.GameCore {
    public class Game : MonoBehaviour {
        public static Action OnGameStart;
        public static Action OnRoundStart;
        /// <summary>
        /// Player - winner of round
        /// </summary>
        public static Action<Player> OnEndRound; 
        public static Action OnEndGame;
        public static Dictionary<PlayerSide, Player> players = new Dictionary<PlayerSide, Player>();
        public static GameSettings Settings { get; private set; }
        public static GameMode gameMode;
        public static bool isPlaying;

        static Bounds? cameraBounds;
        public static Bounds CameraBounds {
            get {
                if (!cameraBounds.HasValue)
                    cameraBounds = CalculateCameraBounds();
                return cameraBounds.Value;
            }
        }

        [SerializeField]
        Player leftPlayer;
        [SerializeField]
        Player rightPlayer;
        [SerializeField]
        Ball ball;
        [SerializeField]
        GameSettings settings;

        void Awake() {
            Settings = settings;
            SetupPlayer(PlayerSide.Left, leftPlayer);
            SetupPlayer(PlayerSide.Right, rightPlayer);
            rightPlayer.transform.rotation = Quaternion.Euler(0, 0, 180);
            OnGameStart += HandleGameStart;
            OnEndRound += HandleEndRound;
            OnEndGame += HandleEndGame;
        }

        void OnDestroy() {
            OnGameStart -= HandleGameStart;
            OnEndGame -= HandleEndGame;
            OnEndRound -= HandleEndRound;
        }

        void HandleEndRound(Player obj) {
            if (leftPlayer.Score < Settings.pointsToWin && rightPlayer.Score < Settings.pointsToWin && isPlaying)
                ball.PrepareNewRound();
            else
                OnEndGame();
        }

        void HandleGameStart() {
            isPlaying = true;
            ball.PrepareNewRound();
        }

        void HandleEndGame() {
            isPlaying = false;
        }

        void SetupPlayer(PlayerSide side, Player player) {
            player.playerSide = side;
            players.Add(side, player);
        }

        public static Bounds CalculateCameraBounds() {
            var camera = Camera.main;
            var aspect = (float)Screen.width / Screen.height;
            var cameraHeight = camera.orthographicSize * 2;
            var cameraWidth = cameraHeight * aspect;
            var size = new Vector3(cameraWidth, cameraHeight, 0);
            return new Bounds(camera.transform.position, size);
        }
    }
}