﻿using System.Collections;
using Pong.GameCore;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControlPreview : MonoBehaviour {
    [SerializeField]
    Image rightPlayerControl;
    [SerializeField]
    Image leftPlayerControl;
    const string leftPlayerPrefsKey = "PlayerLeftPlaysFirstTime";
    const string rightPlayerPrefsKey = "PlayerRightPlaysFirstTime";

    void Start() {
        Game.OnGameStart += HandleGameStart;
        Game.OnRoundStart += HideImages;
        leftPlayerControl.enabled = false;
        rightPlayerControl.enabled = false;
    }

    void OnDestroy() {
        Game.OnGameStart -= HandleGameStart;
        Game.OnRoundStart -= HideImages;
    }

    void HandleGameStart() {
        var firstTimeLeftPlayer = PlayerPrefs.GetInt(leftPlayerPrefsKey, 0);
        var firstTimeRightPlayer = PlayerPrefs.GetInt(rightPlayerPrefsKey, 0);
        leftPlayerControl.enabled = firstTimeLeftPlayer == 0;
        if (Game.gameMode == GameMode.Multiplayer)
            rightPlayerControl.enabled = firstTimeRightPlayer == 0;
        SetPlayerPrefs();
    }

    void HideImages() {
        if (leftPlayerControl.enabled)
            StartCoroutine(HideImage(leftPlayerControl));
        if (rightPlayerControl.enabled)
            StartCoroutine(HideImage(rightPlayerControl));
    }

    IEnumerator HideImage(Image image) {
        while (image.color.a > 0) {
            var colorCopy = image.color;
            colorCopy.a -= Time.deltaTime/2;
            image.color = colorCopy;
            yield return new WaitForEndOfFrame();
        }
    }

    void SetPlayerPrefs() {
        PlayerPrefs.SetInt(leftPlayerPrefsKey, 1);
        if (Game.gameMode == GameMode.Multiplayer)
            PlayerPrefs.SetInt(rightPlayerPrefsKey, 1);
    }
}
