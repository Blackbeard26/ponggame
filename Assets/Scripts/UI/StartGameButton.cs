﻿using Pong.GameCore;
using UnityEngine;
using UnityEngine.UI;

public class StartGameButton : MonoBehaviour {
    [SerializeField]
    bool isRepeatButton;
    [SerializeField]
    public GameMode gameMode;
    Button button;

    void Start() {
        button = GetComponent<Button>();
        button.onClick.AddListener(OnClick);
    }

    void OnDestroy() {
        button.onClick.RemoveListener(OnClick);
    }

    void OnClick() {
        if(!isRepeatButton)
            Game.gameMode = gameMode;
        if (Game.OnGameStart != null)
            Game.OnGameStart();
    }
}
