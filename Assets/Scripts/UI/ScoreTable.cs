﻿using Pong.GameCore;
using System;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTable : MonoBehaviour {
    [SerializeField]
    ScoreElements left;
    [SerializeField]
    ScoreElements right;

    void Start() {
        Game.OnGameStart += ResetTable;
        Game.OnEndRound += HandleEndRound;
        Game.OnEndGame += HandleEndGame;
    }

    void OnDestroy() {
        Game.OnGameStart -= ResetTable;
        Game.OnEndRound -= HandleEndRound;
        Game.OnEndGame -= HandleEndGame;
    }

    void HandleEndRound(Player obj) {
        if (obj.playerSide == PlayerSide.Left)
            left.scoreText.text = obj.Score.ToString();
        else
            right.scoreText.text = obj.Score.ToString();
    }

    public void ResetTable() {
        left.Reset();
        right.Reset();
    }

    void HandleEndGame() {
        var rpScore = Game.players[PlayerSide.Right].Score;
        var lpScore = Game.players[PlayerSide.Left].Score;
        if (lpScore != rpScore) {
            var winner = lpScore > rpScore ? left : right;
            winner.outline.enabled = true;
        }
    }

    [Serializable]
    public class ScoreElements {
        public Text scoreText;
        public Outline outline;

        public void Reset() {
            scoreText.text = "0";
            outline.enabled = false;
        }
    }
}
