﻿using Pong.GameCore;
using UnityEngine;

public class PanelSwitcher : MonoBehaviour {
    [SerializeField]
    GameObject mainPanel;
    [SerializeField]
    GameObject endGamePanel;
    [SerializeField]
    ScoreTable scoreTable;

    void Start() {
        Game.OnGameStart += HandleGameStart;
        Game.OnEndGame += HandleEndGame;
    }

    void OnDestroy() {
        Game.OnGameStart -= HandleGameStart;
        Game.OnEndGame -= HandleEndGame;
    }

    void Update() {
        if (Game.isPlaying && Input.GetKeyDown(KeyCode.Escape)) {
            if (Game.OnEndGame != null)
                Game.OnEndGame();
        }
    }

    void HandleGameStart() {
        mainPanel.SetActive(false);
        endGamePanel.SetActive(false);
    }

    void HandleEndGame() {
        endGamePanel.SetActive(true);
    }

    public void BackToMenu() {
        scoreTable.ResetTable();
        mainPanel.SetActive(true);
        endGamePanel.SetActive(false);
    }

    public void Exit() {
        Application.Quit();
    }
}
