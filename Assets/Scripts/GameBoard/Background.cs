﻿using Pong.GameCore;
using UnityEngine;

public class Background : MonoBehaviour {

	void Start () {
        var sr = GetComponent<SpriteRenderer>();
        StretchToFullScreen(sr);
    }

    void StretchToFullScreen(SpriteRenderer spriteRenderer) {
        var spriteSize = spriteRenderer.sprite.bounds.size;
        spriteRenderer.transform.localScale = new Vector3(Game.CameraBounds.size.x / spriteSize.x, Game.CameraBounds.size.y / spriteSize.y, 1);
    }
}
