﻿using Pong.GameCore;
using UnityEngine;

public class ScoreBorder : DisplayBorder {
    [SerializeField]
    PlayerSide scoreForSide;

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.collider.tag == "Ball") {
            var winner = Game.players[scoreForSide];
            winner.IncrementScore();
            if(Game.OnEndRound != null)
                Game.OnEndRound(winner);
        }
    }
} 
