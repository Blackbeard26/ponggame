﻿using Pong.GameCore;
using UnityEngine;

public class DisplayBorder : MonoBehaviour {
    [SerializeField]
    Vector2 position;
    void Start() {
        var sr = GetComponent<SpriteRenderer>();
        SetScale(sr);
        SetPosition(sr);
    }

    void SetScale(SpriteRenderer spriteRenderer) {
        var spriteSize = spriteRenderer.sprite.bounds.size;
        Vector2 newScale;
        newScale.x = position.y == 0 ? Game.Settings.borderWidth : Game.CameraBounds.size.x / spriteSize.x;
        newScale.y = position.x == 0 ? Game.Settings.borderWidth : Game.CameraBounds.size.y / spriteSize.y;
        spriteRenderer.transform.localScale = newScale;
    }

    void SetPosition(SpriteRenderer spriteRenderer) {
        var spriteSize = Vector3.Scale(spriteRenderer.sprite.bounds.extents, transform.lossyScale);
        Vector2 newPosition;
        newPosition.x = position.y == 0 ? (Game.CameraBounds.min.x) * position.x - spriteSize.x * position.x : 0;
        newPosition.y = position.x == 0 ? (Game.CameraBounds.min.y) * position.y - spriteSize.y * position.y : 0;
        spriteRenderer.transform.position = newPosition;
    }
} 
